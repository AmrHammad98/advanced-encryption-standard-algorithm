import numpy as np
from Constants import *
from textwrap import wrap

def printout(sm):
    sm = np.array(sm)
    out = ""
    for i in range(4):
        x = np.transpose(sm[:,[i]]).tolist()
        for z in x:
            for c in z:
                if len(hex(c)) > 3:
                    out += hex(c)[2:]
                else:
                    out += "0"
                    out += hex(c)[2:]
    print(out)

# a method to convert user input string to a matrix block
# s -> string representing user input
def blockToState(s, c):
    s = wrap(s, 2)                                                                  # split string to diagrams
    state = list()

    # convert into hexadecimal
    for i in s:
        state.append(int(i, 16))

    if c:
        state = np.transpose(np.reshape(np.array(state), (4, 4)))                       # create a state
        state = state.tolist()
    else:
        state = np.reshape(np.array(state), (4, 4)).tolist()

    return state

# a method to implement S_box\sub-byte operation
# state -> given matrix (list of lists) of hexadecimal values representing key, box -> s-box\inv_s-box
def subBytes(state, box):
    x = 0
    for i in state:
        for j in i:
            i[x] = box[j]
            x += 1
        x = 0
    return state

# a method to implement shift rows operation
# sm -> state matrix to perform operation on, c -> choice (True = shift rows, False = inverse shift)
def shiftRows(sm, c):
    if c:
        for i in range(len(sm)):
            x = sm[i][i:] + sm[i][:i]
            sm[i] = x
        return sm
    else:
        j = 0
        for i in range(len(sm), 0, -1):
            x = sm[j][i:len(sm)] + sm[j][:i]
            sm[j] = x
            j+=1
        return sm

# a method to implement Multiplication in the Galois field GF(2^8).
def Gmul(a, b):

    p = 0
    hi_bit_set = 0
    for i in range(8):
        if b & 1 == 1:
            p ^= a
        hi_bit_set = a & 0x80
        a <<= 1
        if hi_bit_set == 0x80:
            a ^= 0x1b
        b >>= 1
    return p % 256

# a method to perform mixing columns
# sm -> state matrix, c -> choice (True = normal, False = Inverse)
def MixColumns(sm, c):
    s = [[0 for i in range(4)] for j in range(4)]
    row = 0
    column = 0
    for x in sm:
        for j in x:
            s[row][column] = j
            column += 1
        column = 0
        row += 1
    if c:
        for i in range(4):
            sm[0][i] = (Gmul(0x02, s[0][i]) ^ Gmul(0x03, s[1][i]) ^ s[2][i] ^ s[3][i])
            sm[1][i] = (s[0][i] ^ Gmul(0x02, s[1][i]) ^ Gmul(0x03, s[2][i]) ^ s[3][i])
            sm[2][i] = (s[0][i] ^ s[1][i] ^ Gmul(0x02, s[2][i]) ^ Gmul(0x03, s[3][i]))
            sm[3][i] = (Gmul(0x03, s[0][i]) ^ s[1][i] ^ s[2][i] ^ Gmul(0x02, s[3][i]))
    else:
        for i in range(4):
            sm[0][i] = (Gmul(0x0e, s[0][i])) ^ (Gmul(0x0b, s[1][i])) ^ (Gmul(0x0d, s[2][i])) ^ (Gmul(0x09, s[3][i]))
            sm[1][i] = (Gmul(0x09, s[0][i])) ^ (Gmul(0x0e, s[1][i])) ^ (Gmul(0x0b, s[2][i])) ^ (Gmul(0x0d, s[3][i]))
            sm[2][i] = (Gmul(0x0d, s[0][i])) ^ (Gmul(0x09, s[1][i])) ^ (Gmul(0x0e, s[2][i])) ^ (Gmul(0x0b, s[3][i]))
            sm[3][i] = (Gmul(0x0b, s[0][i])) ^ (Gmul(0x0d, s[1][i])) ^ (Gmul(0x09, s[2][i])) ^ (Gmul(0x0e, s[3][i]))
    return sm

# a method to perform xor-ing between 2 states to perform the Add Round Key operation
# sm1 -> state matrix 1, sm2 -> state matrix 2
def addRoundKey(sm1, sm2):
    for i in range(4):
        for j in range(4):
            sm1[i][j] ^= sm2[i][j]
    return sm1

# a method to perform key expansion
# sm -> state matrix of cipher key
def expandKey(sm):
    sm = np.array(sm)
    for i in range(4, 44):
        # multiple of 4
        if i%4 == 0:
            wrd = np.transpose(sm[:, [i - 1]])[0].tolist()                            # get column W(i -1)
            y = np.transpose(sm[:, i - 4]).tolist()                                   # get column W(i -4)

            # rot word
            wrd = wrd[1:] + wrd[:1]

            # s_box
            index = 0
            for j in wrd:
                wrd[index] = s_box[j]
                index += 1

            index = 0
            # xor-ing
            for k in wrd:
                if index == 0:
                    wrd[index] = wrd[index] ^ y[index] ^ Rcon[int(i/4)]
                else:
                    wrd[index] = wrd[index] ^ y[index]
                index +=1

            # append to list
            sm = sm.tolist()
            for z in range(4):
                sm[z].append(wrd[z])
            sm = np.array(sm)
        else:
            wrd = np.transpose(sm[:, [i - 1]])[0].tolist()                            # get column W(i -1)
            y = np.transpose(sm[:, i - 4]).tolist()                                   # get column W(i -4)

            index = 0
            # x-oring
            for r in wrd:
                wrd[index] = r ^ y[index]
                index += 1

            # append to list
            sm = sm.tolist()
            for z in range(4):
                sm[z].append(wrd[z])
            sm = np.array(sm)
    return sm

# a method to perform encryption
# ptx -> plaintext\cipher text from user, key -> key from the user, c -> choice (True = encryption, False = decryption)
def AES_encrypt(ptx, key, c):
    # generate state matrix for key and plain text
    if c:
        ptx = blockToState(ptx, True)                   # state matrix of plain text
        key = blockToState(key, True)                   # state matrix of key

    keys = list()                                       # list of round keys

    # expand key
    k1 = expandKey(key)

    for i in range(0,44,4):
        x = k1[:, [i, i+1, i+2, i+3]]
        keys.append(x.tolist())

    # initial transformation
    ptx = addRoundKey(ptx, key)                         # add round key of initial states

    # start rounds (10 rounds)
    for j in range(9):
        sb = subBytes(ptx, s_box)                       # subBytes
        sr = shiftRows(sb, True)                        # shift rows
        mc = MixColumns(sr, True)                       # mix columns
        ptx = addRoundKey(mc, keys[j + 1])              # addRoundKey

    # final round
    fin_sb = subBytes(ptx, s_box)                       # subBytes
    fin_sr = shiftRows(fin_sb, True)                    # shift rows
    ctx = addRoundKey(fin_sr, keys[10])
    return ctx

# a method to perform AES decryption
def AES_decrypt(ctx, key):
    # generate state matrix for key and plain text
    ctx = blockToState(ctx, False)                          # state matrix of cipher text
    key = blockToState(key, True)                           # state matrix of key
    keys = list()                                           # list of round keys

    # expand key
    k1 = expandKey(key)

    for i in range(0,44,4):
        x = k1[:, [i, i+1, i+2, i+3]]
        keys.append(x.tolist())

    # initial transformation
    ctx = addRoundKey(ctx, keys[10])                        # add round key of initial states

    # start rounds (10 rounds)
    for j in range(9, 0, -1):
        inv_sr = shiftRows(ctx, False)                      # inverse shift rows
        inv_sb = subBytes(inv_sr, inv_s_box)                # inverse sub bytes
        add = addRoundKey(inv_sb, keys[j])                  # add Round key
        ctx = MixColumns(add, False)                        # inverse mix columns

    # final round
    fin_sr = shiftRows(ctx, False)                          # final inverse shift rows
    fin_sb = subBytes(fin_sr, inv_s_box)                    # final inverse sub bytes
    ptx = addRoundKey(fin_sb, key)                          # plain text
    return ptx