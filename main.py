from AES import *

if __name__ == "__main__":
    # Key input from user
    key = input("Please enter 32 Hexadecimal Characters for key\n")

    # plain text from the user
    pt = input("Please enter 32 Hexadecimal Characters for plaintext encryption\n")

    # cipher text from user
    ct = input("Please enter 32 Hexadecimal Characters for cipher-text decryption\n")

    # encrypt
    cipher = AES_encrypt(key, key, True)
    print("Cipher-Text:")
    printout(cipher)

    # decrypt
    plain = AES_decrypt(ct, key)
    print("Plain-Text:")
    printout(plain)
